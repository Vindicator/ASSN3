#include<stdio.h>

int main_menu(); //메인 메뉴를 출력하고 선택지를 입력받아 반환하는 함수
int char_to_color(char a); //문자가 입력된 변수를 밝기로 변환하여 반환하는 함수
int import_file(int a[7][70]);//파일의 이름을 입력받고 해당 파일을 IMPORT하여 프로그램 내부 공간으로 저장하는 함수
void init(int a[7][70], int b);//2차원 배열을 특정 정수 값으로 초기화하는 함수
void export_file(int a[7][70]);//파일의 이름을 입력받고 해당 이름으로 저장된 이미지를 EXPORT하는 함수
void print_image(int a[7][70]); //밝기가 입력된 2차원 배열을 상응하는 문자로 출력하는 함수
void make_valid(int *a); //변수에 저장된 숫자가 범위를 초과할 시 양 극값으로 변경하는 함수
void brighter(int a[7][70]); //2차원 배열의 밝기를 증가시키는 함수
void darker(int a[7][70]); //2차원 배열의 밝기를 감소시키는 함수
void sharpen(int a[7][70]); //2차원 배열에서 픽셀별로 이미지 Sharpening을 수행하는 함수

  int main(void)
{
	int cho;
	int img[7][70]; //프로그램 내에서 이미지를 밝기로 저장할 2차원 배열 선언
	init(img, 1);
	while (1) //종료를 입력하지 않는 한 메뉴를 다시 불러오기 위해 무한반복문 삽입
	{
		cho = main_menu(); //main_menu 함수로부터 선택지를 가져온다.
		if (cho == 1)//현재 프로그램에 저장된 이미지를 출력
		{
			print_image(img);
			continue;
		} 
		else if (cho == 2)//현재 프로그램에 저장된 이미지를 파일로 출력
		{
			export_file(img);
			continue;
		}
		else if (cho == 3)//파일을 입력받아 그 데이터를 프로그램에 저장하고 화면에 출력
		{
			if (!import_file(img))
				print_image(img);
			continue;
		}
		else if (cho == 4)//파일의 밝기를 1 감소 후 화면에 출력
		{
			darker(img);
			print_image(img);
			continue;
		}
		else if (cho == 5)//파일의 밝기를 1 증가 후 화면에 출력
		{
			brighter(img);
			print_image(img);
			continue;
		}
		else if (cho == 6)//파일의 픽셀들에 대해 sharpening 작업을 수행 후 화면에 출력
		{
			sharpen(img);
			print_image(img);
			continue;
		}
		else if (cho == 0)//프로그램 종료를 위해 무한 반복문을 탈출
		{
			break;
		}
		else//기타 숫자를 입력했을 경우 에러 메시지를 표시하고 다시 메뉴 출력
		{
			printf("Input Error\n");
			continue;
		} 
	} 
	return 0;//프로그램 종료
}

int main_menu()
{
	int choice;
	printf("============================\n");
	printf("1. Print Image\n");
	printf("2. Save Image\n");
	printf("3. Load Image\n");
	printf("4. Make image darker\n");
	printf("5. Make image brighter\n");
	printf("6. Sharpen image\n");
	printf("0. Quit\n");
	printf("============================\n");
	printf(">");
	scanf("%d", &choice);
	printf("\n");
	return choice;
}
int char_to_color(char a)
{
	char colors[11] = " .-:=+*#%@";//밝기에 상응하는 문자를 저장하는 1차원 배열 선언
	int i;
	for (i = 0; i < 10; i++)
		if (a == colors[i])
			return i;
}
void print_image(int a[7][70])
{
	int i, j;
	char colors[11] = " .-:=+*#%@";//밝기에 상응하는 문자를 저장하는 1차원 배열 선언. 
	for (i = 0; i < 7; i++)
	{
		for (j = 0; j < 70; j++)
		{
			make_valid(&a[i][j]);//밝기 범위를 초과하는 픽셀에 대해 가장 가까운 허용된 픽셀 범위로 변환
			printf("%c", colors[a[i][j]]); //각 픽셀에 대해 밝기와 index의 밝기와 값을 문자로 출력

		}
		printf("\n");
	}
	printf("\n");
}
void init(int a[7][70], int b)
{
	int i, j;
	for (i = 0; i < 7; i++)//2차원 배열의 모든 요소를 1로 초기화하는 2중 반복문 시행
		for (j = 0; j < 70; j++)
			a[i][j] = b;
}
void make_valid(int *a)
{
	if (*a > 9)
	{
		*a = 9;
	}
	else if (*a < 0)
	{
		*a = 0;
	}
}
void brighter(int a[7][70])
{
	int i, j;
	for (i = 0; i < 7; i++)
	{
		for (j = 0; j < 70; j++)
		{
			a[i][j] += 1;
		}
	}
}
void darker(int a[7][70])
{
	int i, j;
	for (i = 0; i < 7; i++)
	{
		for (j = 0; j < 70; j++)
		{
			a[i][j] -= 1;
		}
	}
}
void sharpen(int a[7][70])
{
	int i, j;
	int x, y, z, w;
	int b[7][70];
	for (i = 0; i < 7; i++)
		for (j = 0; j < 70; j++)
		{
			if (i == 6)//이미지의 하단부에서 예외를 처리하는 명령어
			{
				x = 0;
				z = 1;
			}
			else if (i == 0)//이미지의 상단부에서 예외를 처리하는 명령어
			{
				z = 0;
				x = 1;
			}
			else//최상단, 최하단을 제외한 나머지 케이스를 커버하는 명령어
			{
				z = 1;
				x = 1;
			} 

			if (j==69)//우측 모서리 예외를 처리하는 명령어
			{
				y= 0;
				w = 1;
			}
			else if (j == 0)//좌측 모서리 예외를 처리하는 명령어
			{
				w = 0;
				y = 1;
			}
			else //좌측, 우측 모서리를 제외한 나머지 케이스를 커버하는 명령어
			{
				y = 1;
				w = 1;
			}
			b[i][j] = 5 * a[i][j] - (x * a[i + 1][j] + y * a[i][j + 1] + z * a[i - 1][j] + w * a[i][j - 1]);//임시 배열에 값을 저장
		}
	for (i = 0; i < 7; i++)
		for (j = 0; j < 70; j++)
		{
			a[i][j] = b[i][j];//임시 배열에 저장된 값을 원래 배열로 반환
		}
}
int import_file(int a[7][70])
{
	FILE *infile;//파일 import를 위한 포인터 선언
	int i, j, chk;
	char inpf[21];//import할 파일 이름을 저장할 문자열
	char ipt;
	printf("Insert filename to load from.\n");
	printf(">");
	scanf("%s", inpf);//파일 이름을 입력받음
	infile = fopen(inpf, "r");
	if (infile == NULL)//파일을 read할 수 없는 경우 필터링
	{
		printf("Cannot open file.\n");
		return 1;
	}
	else
	{
		printf("Succesfully loaded, displaying image:\n");
		for (i = 0; i < 7; i++)
		{
			for (j = 0; j < 70; j++)
			{
				chk = fscanf(infile, "%c", &ipt);//파일에서 를 읽는 동시에 끝을 확인하기 위해 다른 변수에 fscanf의 반환값을 대입
				if (chk == EOF)//chk에 저장된 반환값이 EOF일 경우 파일의 끝으로 간주하여 반복문 종료
					break;
				else
					a[i][j] = char_to_color(ipt);//입력받은 문자 형태의 픽셀을 밝기로 변환하여 프로그램에 저장
			}
			fscanf(infile, "%c", &ipt);
			if (chk == EOF)//chk에 저장된 반환값이 EOF일 경우 파일의 끝으로 간주하여 반복문 종료
				break;
		}
		fclose(infile);
		return 0;
	}
}
void export_file(int a[7][70])
{
	FILE *outfile;//파일 export를 위한 파일 포인터 선언
	int i, j;
	char outf[21];//export할 파일 이름을 입력받을 문자열
	char colors[11] = " .-:=+*#%@";
	printf("Insert filename to save to.\n");
	printf(">");
	scanf("%s", outf);//출력 파일의 이름을 입력받음
	outfile = fopen(outf, "w");//파일명을 입력받아 출력을 위한 파일을 쓰기 모드로 열기
	if (outfile == NULL)
	{
		printf("Cannot open file.\n");
	}
	else
	{
		for (i = 0; i < 7; i++)//문자로 변환된 데이터를 파일에 쓰기
		{
			for (j = 0; j < 70; j++)
			{
				fprintf(outfile, "%c", colors[a[i][j]]);
			}
			fprintf(outfile, "\n");
		}
		fclose(outfile);
	}
	printf("\n");
}