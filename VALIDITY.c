#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


int show_menu();
int char_to_color(char a, char colors[10] );
int make_valid(int a);
void print_image(int img[7][70], char colors[10]);
void load_image(int img[7][70]);
void bright_image(int img[7][70]);

int main() // 모든 scanf_s를 마지막에 scanf로 바꿔주기!
{
   int num, i, j;
   char colors[10] = { " .-:=+*#%@" };
   int img[7][70];

   for (i = 0; i < 7; i++)
   {
      for (j = 0; j < 70; j++)
      {
         img[i][j] = colors[1];
      }
   }

   //show_menu();
   while (1)
   {
      num = show_menu();

      switch (num)
      {
      case 1:

         print_image(img, colors);
         break;
         //show_menu();

      case 2:

         //show_menu;

      case 3:

         load_image(img);
         print_image(img, colors);
         //show_menu();

      case 4:
         bright_image(img);
         print_image(img, colors);
         //show_menu();

      case 5:

         //show_menu();

      case 6:

         break;

      case 0:
         break;

      default:
         printf("다시 입력하세요 :\n");
         show_menu();

      }
   }// while(1)



   return 0;
}

int show_menu()
{
   int num;

   printf("============================\n");
   printf("1. Print Image\n");
   printf("2. Save Image\n");
   printf("3. Load Image\n");
   printf("4. Make image darker\n");
   printf("5. Make image brighter\n");
   printf("6. Sharpen image\n");
   printf("0. Quit\n");
   printf("============================\n");
   printf(">");

   scanf("%d", &num);

   return num;
}

int char_to_color(char a, char colors[10]) 
{
   int index, num=-1;
   for (index = 0; index < 10; index++)
   {
      if (a == colors[index])
      {
         num = index;
         break;
      }
   }

   return num;
}

int make_valid(int a)
{
   if ((a >= 0) && (a <= 9))
   {
      return a;
   }
   else if (a < 0)
   {
      return 0;
   }
   else if (a > 9)
   {
      return 9;
   }

}


void print_image(int img[7][70], char colors[10])
{
   int i, j;
   char print_image[7][70];
   for (i = 0; i < 7; i++)
   {
      for (j = 0; j < 71; j++)
      {   
         print_image[i][j] = colors[img[i][j]];
         printf("%c", print_image[i][j]);
      }
      printf("\n");
   }


}

void load_image(int img[7][70])
{
   FILE *infile;
   int i, j;
   char ch;
   char name[20];
   char colors[11] = " .-:=+*#%@";
   
    printf("\nInsert filename to load from \n>");
      scanf("%s", name);
      infile = fopen(name, "r");

      if (infile == NULL)
      {
         printf("Cannot open file.\n");
      }
      else
      {
         for (i = 0; i < 7; i++)
         {
            for (j = 0; j < 71; j++)
            {
               while (fscanf(infile, "%c", &ch) != EOF)
               {
                  char_to_color(ch, colors);
                  img[i][j] = char_to_color(ch, colors);
               }
            }
         }
      }
      fclose(infile);
}

void bright_image(int img[7][70])
{
   int i, j;

   for (i = 0; i < 7; i++)
   {
      for (j = 0; j < 71; j++)
      {
         img[i][j] = img[i][j] + 1;
      }
   }
}