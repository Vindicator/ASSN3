#include<stdio.h>

int main(void)
{
    int a[7][70];
    int b;
    b=import_file(a);
    print_image(a);
    return 0;
}

int char_to_color(char a)
{
	char index[11] = " .-:=+*#%@";//밝기에 상응하는 문자를 저장하는 1차원 배열 선언
	int i;
	for (i = 0; i < 10; i++)
		if (a == index[i])
			return i;
}
void make_valid(int *a)
{
	if (*a > 9)
	{
		*a = 9;
	}
	else if (*a < 0)
	{
		*a = 0;
	}
}

void print_image(int a[7][70])
{
	int i, j, k;
	char index[11] = " .-:=+*#%@";//밝기에 상응하는 문자를 저장하는 1차원 배열 선언. 
	for (i = 0; i < 7; i++)
	{
		for (j = 0; j < 70; j++)
		{
			make_valid(&a[i][j]);//밝기 범위를 초과하는 픽셀에 대해 가장 가까운 허용된 픽셀 범위로 변환
			for (k = 0; k < 10; k++)
			{
				if (a[i][j] == k)
					printf("%c", index[k]);//각 픽셀에 대해 밝기와 index의 일치하는 값을 문자로 출력
			}

		}
		printf("\n");
	}
	printf("\n");
}
int import_file(int a[7][70])
{
	FILE *infile;//파일 import를 위한 포인터 선언
	int i, j, chk;
	char inpf[21];//import할 파일 이름을 저장할 문자열
	char ipt;
	printf("Insert filename to load from.\n>");
	//printf(">");
	scanf("%s", inpf);//파일 이름을 입력받음
	infile = fopen(inpf, "r");
	if (infile == NULL)//파일을 read할 수 없는 경우 필터링
	{
		printf("Cannot open file.\n");
		return 1;
	}
	else
	{
		printf("Succesfully loaded, displaying image:\n");
		for (i = 0; i < 7; i++)
		{
			for (j = 0; j < 70; j++)
			{
				chk = fscanf(infile, "%c", &ipt);//파일에서 index를 읽는 동시에 끝을 확인하기 위해 다른 변수에 fscanf의 반환값을 대입
				if (chk == EOF)//chk에 저장된 반환값이 EOF일 경우 파일의 끝으로 간주하여 반복문 종료
					break;
				else
					a[i][j] = char_to_color(ipt);//입력받은 문자 형태의 픽셀을 밝기로 변환하여 프로그램에 저장
			}
			fscanf(infile, "%c", &ipt);
			if (chk == EOF)//chk에 저장된 반환값이 EOF일 경우 파일의 끝으로 간주하여 반복문 종료
				break;
		}
		fclose(infile);
		return 0;
	}
}
